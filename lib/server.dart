// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.
part of c7core;

/*
final Collection<Site> sites = new Collection<Site>(db);
final Collection<User> users = new Collection<User>(db);
final ManyToMany<User,Site> usersSites = new ManyToMany<User,Site>;
*/

class Server{

  List<Module> modules;
  Router router;

  Server(){
    print("initialising server...");
    router = new Router();
    modules = new List<Module>();
  }

  Future serve() async {
    print("starting server...");
    print("checking DB...");

    //TODO
    if (Config.devServer) {
      print("DEV MODE DB CHECK");
      var queries = await Migrator.quessMigrationQueries();
      for (var sql in queries){
        print("running query:\n$sql\n");
        await sites.db.query(sql);
        print("DONE");
      }
    }

    HttpServer server = await HttpServer.bind(InternetAddress.ANY_IP_V4, 7777);
    print("listening on http://127.0.0.1:7777");


    server.listen((HttpRequest request) async {
      int start = new DateTime.now().millisecondsSinceEpoch;
      try {
        HttpAction action = await router.handle(request);
        //await dataRead
        //await action.data();

        request.response.headers.contentType = action.contentType;
        //request.response.headers.set('', action.contentType);
        String body = action.render();
        request.response.write(body);
      } catch (error, stackTrace) {
        if (error is Error404) {
          request.response.statusCode = 404;
          request.response.headers.contentType = ContentType.HTML;
          request.response.write("<h1>404 ERROR</h1>");
        } else if (error is Redirect) {
          request.response.redirect(new Uri.http(request.uri.authority, error.uri));
        } else {
          request.response.statusCode = 500;
          request.response.headers.contentType = ContentType.HTML;
          request.response.write("<h1>500 ERROR</h1>");
          request.response.write("<pre>${new HtmlEscape().convert(error.toString())}</pre>");
          request.response.write("<pre>${new HtmlEscape().convert(stackTrace.toString())}</pre>");

        }
      }
      request.response.close();
      print("${request.method} ${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");

    });
    print("shutting down...");
  }
}
