// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

abstract class Content {

  String get title;
  String get description;
  String get keywords;
  
}
