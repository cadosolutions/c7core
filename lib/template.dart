part of c7core;

class Theme {
  Uid uid;
  One<Site> site;
  String less;
  String dart;
}

class Template implements ITemplate {
  Uid uid;
  One<Site> site;
  One<Template> parent;
  Template get baseTemplate => parent.fetch;
  //dataType
  String templateBody;
}
