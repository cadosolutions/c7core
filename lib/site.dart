// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

var sites = new Collection<Site>(db);

class Site implements Content {

  Uid uid;

  @UniqueKey(#domain)
  String domain;

  String name;

  String description;
  String keywords;

  String get title => name;

}
