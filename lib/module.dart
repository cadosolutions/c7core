// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

//plugable / switchable module
class Module {

  Collection<ModuleSettings> settings;

  static List<Module> allModules() {
    final mirror = currentMirrorSystem();
    mirror.libraries.forEach((uri, libMirror){
      //print(uri);
      libMirror.declarations.forEach((name, DeclarationMirror declarationMirror) {
        if((declarationMirror is VariableMirror) && (declarationMirror.type is ClassMirror)){
              ClassMirror parentMirror = declarationMirror.type;
              do {
                if (parentMirror.qualifiedName == #c7.Collection){
                  //print('C');
                  //print(declarationMirror.runtimeType);
                  //force calling constructor
                  Collection c = libMirror.getField(declarationMirror.simpleName).reflectee;
                  ret[MirrorSystem.getName(c._mirror.qualifiedName).replaceAll('.', '_')] = this.getTableSchema(c._mirror);

                }
              } while ((parentMirror = parentMirror.superclass) != null);
            }
          });
        });

  }

  Module(){

  }

}


class ModuleSettings {
    One<Site> site;


}
