// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7core;

import 'dart:mirrors';
import 'dart:io';
import 'dart:async';

import 'package:c7orm/c7orm.dart';
import 'package:c7liquid/c7liquid.dart';

import 'dart:convert' show HtmlEscape;

part 'config.dart';
part 'actions.dart';
part 'router.dart';
part 'server.dart';
part 'content.dart';
part 'site.dart';
part 'user.dart';
part 'module.dart';
part 'models.dart';
part 'flatpage.dart';
part 'template.dart';


class CoreModule extends Module {

  Collection<FlatPage> flatPages;
  Collection<ModuleSettings> settings;
  Collection<Site> sites;

  Collection<Theme> themes;
  Collection<Template> templates;

  CoreModule(){
    flatPages = new Collection<FlatPage>(db);
    settings = new Collection<ModuleSettings>(db);
    sites = new Collection<Site>(db);
    themes = new Collection<Theme>(db);
    templates = new Collection<Template>(db);

  }

}
