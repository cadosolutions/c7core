// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

class Url {
  noSuchMethod(Invocation msg) {
    List<String> pathBits = [];
    pathBits.addAll(
        MirrorSystem.getName(msg.memberName)
          .split("Action")
            .where((e) => !e.isEmpty)
              .map((e) => e.toLowerCase())
    );
    pathBits.addAll(msg.positionalArguments);
    String url = "/${pathBits.join("/")}.html";
    if(msg.positionalArguments.isEmpty) url = url.replaceAll("index.html", "");
    print(msg.positionalArguments.isEmpty);

    return url;
  }
}

Url url = new Url();
