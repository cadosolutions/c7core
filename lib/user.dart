// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

class User {

  Uid uid;

  @EmailField()
  String email;

  @PasswordField()
  String password;

  @Relation()
  Site site;

  @VerboseName("First Name")
  @Description("first name of this user")
  //@DefaultInput<String>() //default
  @MaxLength(5)
  @MinLength(5)
  @WriteAccess()
  @ReadAccessUser
  String firstName;

  String lastName;

  String externalEmail;

  Text bio;

  GeoLocation geoLocation;
  GeoLocation lastLoginFrom;


}



class Admin {

}
