// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

class _UrlParam { const _UrlParam(); }
const UrlParam = const _UrlParam();
class _GetParam { const _GetParam(); }
const GetParam = const _GetParam();

class UrlPattern{
  final RegExp pattern;
  const UrlPattern(this.pattern);
}

class Redirect implements Exception {
  String uri;
  Redirect(this.uri);
}

class Error404 implements Exception {

  Error404();

}

class Endpoint {
  ClassMirror actionMirror;
  List<VariableMirror> pathParams;
  List<VariableMirror> getParams;
  List<VariableMirror> postParams;
  Endpoint(this.actionMirror, this.pathParams, this.getParams, this.postParams);
  String toString() =>
      "${MirrorSystem.getName(this.actionMirror.simpleName)}(${
          this.pathParams.map((v)=>
            "${MirrorSystem.getName(v.type.simpleName)} ${MirrorSystem.getName(v.simpleName)}"
          ).join(',')
        })";
}

/**
 * This class is responsible for finding specific HttpAction class from string request and vice versa
 */
class Router {

  Map<RegExp, Endpoint> routingMap;

  Router() {
    prepareRouting();
    routingMap.forEach((e,a){
      print('${e.pattern} => ${a}');
    });
  }

  void prepareRouting() {
    routingMap = new Map<RegExp, Endpoint>();
    final mirror = currentMirrorSystem();
    mirror.libraries.forEach((uri, libMirror) {
      libMirror.declarations.forEach((name, DeclarationMirror declarationMirror) {
        if (declarationMirror is ClassMirror && !declarationMirror.isAbstract && declarationMirror.isSubclassOf(reflectClass(HttpAction))) {
          ClassMirror classMirror = declarationMirror;
          List<String> pathBits = [];
          pathBits.addAll(
              MirrorSystem.getName(declarationMirror.simpleName)
                .split("Action")
                .where((e) => !e.isEmpty)
                .map((e) => e.toLowerCase())
          );
          List<VariableMirror> urlParams = [];
          for (var mirror in AnnotatedMember.getInstanceVariables(classMirror)){
            if(mirror.metadata.map((e)=>e.reflectee).contains(UrlParam)){
              if(mirror.type.simpleName == #int){
                pathBits.add(r"(\d*)");
              } else {
                pathBits.add(r"(.*)");
              }
              //TODO find default collection for type
              urlParams.add(mirror);
            }
          };

          RegExp exp = new RegExp("^/${pathBits.join('/')}.html\$".replaceAll("index.html", ""));
          routingMap[exp] = new Endpoint(declarationMirror, urlParams, null, null);
        }
      });
    });
  }

  Future<String> getPostMap(HttpRequest request) async {
    return await request.listen((post){
        var r = QueryString.parse(new String.fromCharCodes(post));
        action.post = r;
        action.message = action.onPost();
        print('A');
      }, onError: (error){
        print(error);
      }).asFuture();
  }

  Future<HttpAction> handle(HttpRequest request) async {
    String uri = request.uri.toString();
    //print(request.headers.value('X-Forwarded-Host'));
    String host = request.headers.value('Host');
    if (Config.devServer) { //devmode
      int p = uri.indexOf('/', 1);
      if (p != -1) {
        host = uri.substring(1, p);
        uri = uri.substring(p);
      } else {
        //dev mode super index action
        return new DevIndexAction();
      }
    }

    for (final exp in routingMap.keys){
      Match m = exp.matchAsPrefix(uri);
      if (m != null) {
        //print(routingMap[exp].actionClassMirror);
        HttpAction action = routingMap[exp].actionMirror.newInstance(new Symbol(''), []).reflectee;
        action.request = request;
        action.site = null;
        //matching endpoint found!
        for (int i=0; i< routingMap[exp].pathParams.length; i++) {
          //TODO this will be probably slow as fuck
          VariableMirror paramMirror = routingMap[exp].pathParams[i];
          //print(paramMirror.type.simpleName);
          if (paramMirror.type.simpleName == #int) {
            reflect(action).setField(paramMirror.simpleName, int.parse(m.group(i+1)));
          } else if(paramMirror.type.simpleName == #String) {
            reflect(action).setField(paramMirror.simpleName, m.group(i+1));
          } else {
            ClassMirror paramClass = paramMirror.type;
            reflect(action).setField(
              paramMirror.simpleName,
              await Collection.getCollection(paramClass.qualifiedName).getByKey(#url, [action.site, m.group(i+1)])
            );
          }
        }
        if (request.method == 'POST') {
          //var r = QueryString.parse(q);
          action.post = getPostMap(request);
        }
        //await action.data();

        return action;
      }
    };
    throw new Error404();
  }

}
